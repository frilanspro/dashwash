<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>sign-up</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css"
          integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sign-up.css') }}">
</head>
<body>

<section class="sign_up_page">

    <div class="sign_up">

<div class="logo">
    <img src="{{ asset('images/Vector-Smart-Object.png') }}">
</div>

        <form id="input_login" method="POST" action="{{ route('login') }}">@csrf
            <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror


        <button class="login_buttton">Login</button>
        </form>
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
    </div>
    <div class="image_auto">
<div class="image">
    <img src="{{ asset('images/imj.png') }}">
</div>
    </div>
</section>
<script src="{{ asset('libs/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('libs/js/popper.min.js') }}"></script>
<script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
<script src="{{ asset('libs/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('libs/js/wow.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>
