@extends('user.layouts.app')
@section('content')
    <section class="service active" data-content="1">
        <div class="service-flex">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active service_listing" id="home-tab" data-toggle="tab" href="#home" role="tab"
                       aria-controls="home"
                       aria-selected="true">
                        <div class="car"><img src="images/car.png"></div>
                        <p>Statistics of cars</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link service_listing" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                       aria-controls="profile" aria-selected="false">
                        <div class="car">
                            <img src="images/time wash.png">
                        </div>
                        <p>Wash time </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link service_listing" id="messages-tab" data-toggle="tab" href="#messages" role="tab"
                       aria-controls="messages" aria-selected="false">
                        <div class="car">
                            <img src="images/analytics.png">
                        </div>
                        <p>User statistics </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link new_service" id="settings-tab" data-toggle="tab" href="#settings" role="tab"
                       aria-controls="settings" aria-selected="false">
                        <div class="car">
                            <span class="far fa-calendar-alt"></span>
                        </div>
                        <p> Today’s statistics</p>
                    </a>
                </li>
            </ul>
        </div>
    </section>

    <section class="service" data-content="2">
        <div class="service-flex">
            <div class="service_listing">
                <a href="#">
                    <div class="clipboard">
                        <img src="images/clipboard1.png">
                    </div>
                    <p>Employee list and details</p>
                </a>
            </div>
            <div class="new_service">
                <div class="employee_modal" data-toggle="modal" data-target="#exampleModalCenter"><img
                        src="images/add-circular-outlined-button.png">
                    <p>New employee enrollment</p>
                </div>
            </div>
        </div>
    </section>

    <section class="service" data-content="3">
        <div class="service-flex">
            <div class="service_listing">
                <a href="#">
                    <div class="automatic">
                        <img src="images/automatic-wash-car.png">
                    </div>
                    <p>Washing bay listing</p></a>
            </div>
            <div class="new_service">
                <a href="#">
                    <img src="images/add-circular-outlined-button.png">
                    <p>New bay enrollment</p>
                </a>
            </div>
        </div>
    </section>

    <section class="service" data-content="4">
        <div class="service-flex">
            <div class="service_listing">
                <a href="#">
                    <div class="automatic">
                        <img src="images/analytics.png">
                    </div>
                    <p>User listing</p></a>
            </div>
            <div class="new_service">
                <a href="#">
                    <span class="far fa-calendar-alt"></span>
                    <p>Statistics</p>
                </a>
            </div>
        </div>
    </section>

    <section class="service" data-content="5">
        <div class="service-flex">
            <div class="service_listing">
                <a href="#">
                    <div class="automatic">
                        <img src="images/automatic-wash-car.png">
                    </div>
                    <p>Service listing</p></a>
            </div>
            <div class="new_service">
                <a href="#">
                    <img src="images/add-circular-outlined-button.png">
                    <p>New service enrollment</p>
                </a>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab"
               data-page="v-pills-home" data-tab="1"
               aria-controls="v-pills-home" aria-selected="true">
                <img src="images/computer.png">
                <p>Dashboard</p></a>
            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab"
               data-tab="2"
               aria-controls="v-pills-profile" aria-selected="false">
                <img src="images/group.png">
                <p>Employee management</p>
            </a>
            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab"
               data-tab="3"
               aria-controls="v-pills-messages" aria-selected="false">
                <img src="images/document.png">
                <p>Washing bay</p>
            </a>
            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab"
               data-tab="4"
               aria-controls="v-pills-settings" aria-selected="false">
                <img src="images/user-list.png">
                <p>User list</p>
            </a>
            <a class="nav-link" id="v-pills-service-tab" data-toggle="pill" href="#v-pills-service" role="tab"
               data-tab="5"
               aria-controls="v-pills-service" aria-selected="false">
                <img src="images/support.png" class="support">
                <p>Service</p>
            </a>
        </div>

        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <section class="statics_for_cars">
                            <div id="container" style="min-width: 300px; height: 400px; margin: 0  auto"></div>

                            <section class="statistic">
                                <div class="box">
                                    <div class="squar active">

                                    </div>
                                    <p>All cars</p>
                                </div>
                                <div class="box">
                                    <div class="squar">

                                    </div>
                                    <p>SUV</p>
                                </div>
                                <div class="box">
                                    <div class="squar">

                                    </div>
                                    <p>sedan</p>
                                </div>
                                <div class="box">
                                    <div class="squar">

                                    </div>
                                    <p>truck</p>
                                </div>
                            </section>


                        </section>
                    </div>
                    <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <section class="average_car_wash">
                            <h2>Average car wash time </h2>
                            <table class="table table-responsive text-nowrap">
                                <thead>
                                <tr>
                                    <th scope="col">Username</th>
                                    <th scope="col">Check-in</th>
                                    <th scope="col">Check-out</th>
                                    <th scope="col">Time spent washing</th>
                                    <th>

                                        <div class="dropdown">
                                            <div class="data"><span class="fas calendar fa-calendar-alt"></span>
                                                <p class="datea"></p></div>
                                            <div class="dropdown-content">
                                                <div id="calendar-container1">
                                                    <h1 id="calendar-title1">
                                                        <div class="btn left"></div>
                                                        <span class="dropdata"></span>
                                                        <div class="btn right"></div>
                                                    </h1>
                                                    <table id="calendar-table1">
                                                        <tr>
                                                            <th>Sun</th>
                                                            <th>Mon</th>
                                                            <th>Tue</th>
                                                            <th>Wed</th>
                                                            <th>Thu</th>
                                                            <th>Fri</th>
                                                            <th>Sat</th>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>1</td>
                                                            <td>2</td>
                                                            <td>3</td>
                                                            <td>4</td>
                                                            <td>5</td>
                                                            <td>6</td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td>8</td>
                                                            <td>9</td>
                                                            <td>10</td>
                                                            <td>11</td>
                                                            <td>12</td>
                                                            <td>13</td>
                                                        </tr>
                                                        <tr>
                                                            <td>14</td>
                                                            <td>15</td>
                                                            <td>16</td>
                                                            <td>17</td>
                                                            <td>18</td>
                                                            <td>19</td>
                                                            <td>20</td>
                                                        </tr>
                                                        <tr>
                                                            <td>21</td>
                                                            <td>22</td>
                                                            <td>23</td>
                                                            <td>24</td>
                                                            <td>25</td>
                                                            <td>26</td>
                                                            <td>27</td>
                                                        </tr>
                                                        <tr>
                                                            <td>28</td>
                                                            <td>29</td>
                                                            <td>30</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>

                                                    </table>
                                                    <p id="date-text1"></p>
                                                </div>

                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><span class="far fa-user"></span>
                                        Name and Surname
                                    </td>
                                    <td>11:22:02</td>
                                    <td>12:02:02</td>
                                    <td class="time"> 57 min</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </section>
                    </div>
                    <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">
                        <section>
                            <div class="user_statistics">
                                <h2>User statistics</h2>
                                <div class="search_input d-flex">
                                    <form class="input_area">
                                        <input type="search" placeholder="Enter the car's license plate to search">
                                        <span class="fas fa-search"></span>
                                    </form>
                                </div>
                            </div>
                            <table class="table table-responsive text-nowrap">
                                <thead>
                                <tr>
                                    <th scope="col">Username</th>
                                    <th scope="col">Car license plate</th>
                                    <th scope="col">Check-in</th>
                                    <th scope="col">Check-out</th>
                                    <th scope="col">Time spent washing</th>

                                    <th>
                                        <div class="dropdown">
                                            <div class="data"><span class="fas calendar fa-calendar-alt"></span>
                                                <p class="datea"> 12 April 2019 </p></div>
                                            <div class="dropdown-content">
                                                <div id="calendar-container">
                                                    <h1 id="calendar-title">
                                                        <div class="btn left"></div>
                                                        <span class="dropdata">April, 2019</span>
                                                        <div class="btn right"></div>
                                                    </h1>
                                                    <table id="calendar-table">
                                                        <tr>
                                                            <th>Sun</th>
                                                            <th>Mon</th>
                                                            <th>Tue</th>
                                                            <th>Wed</th>
                                                            <th>Thu</th>
                                                            <th>Fri</th>
                                                            <th>Sat</th>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>1</td>
                                                            <td>2</td>
                                                            <td>3</td>
                                                            <td>4</td>
                                                            <td>5</td>
                                                            <td>6</td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td>8</td>
                                                            <td>9</td>
                                                            <td>10</td>
                                                            <td>11</td>
                                                            <td>12</td>
                                                            <td>13</td>
                                                        </tr>
                                                        <tr>
                                                            <td>14</td>
                                                            <td>15</td>
                                                            <td>16</td>
                                                            <td>17</td>
                                                            <td>18</td>
                                                            <td>19</td>
                                                            <td>20</td>
                                                        </tr>
                                                        <tr>
                                                            <td>21</td>
                                                            <td>22</td>
                                                            <td>23</td>
                                                            <td>24</td>
                                                            <td>25</td>
                                                            <td>26</td>
                                                            <td>27</td>
                                                        </tr>
                                                        <tr>
                                                            <td>28</td>
                                                            <td>29</td>
                                                            <td>30</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>

                                                    </table>
                                                    <p id="date-text"></p>
                                                </div>

                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><span class="far fa-user"></span>
                                        Name and Surname
                                    </td>
                                    <td> AAA-10-00</td>
                                    <td>11:22:02</td>
                                    <td>12:02:02</td>
                                    <td class="time"> 57 min</td>
                                    <td class="icons_text text-right">
                                        <i class="far fa-edit"></i>
                                        <i class="fas fa-trash-alt"></i>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </section>
                    </div>

                    <div class="tab-pane" id="settings" role="tabpanel" aria-labelledby="settings-tab">

                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <h2>Employee list and details</h2>

                <div class="table-responsive text-nowrap">
                    <form action="">
                        @csrf
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Work day</th>
                                <th scope="col">Working hours</th>
                                <th scope="col">Status</th>
                                <th scope="col">Employee id</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($employees as $employee)
                                <tr class="employee{{ $employee->id }}">
                                    <td>
                                        <span class="far fa-user"></span>
                                        <span class="name" data-value="{{ $employee->first_name }}">
                                            {{ $employee->first_name }}
                                        </span>
                                    </td>
                                    <td class="last_name"
                                        data-value="{{ $employee->last_name }}">{{ $employee->last_name }}</td>
                                    <td class="date" data-value="{{ $employee->date }}">{{ $employee->date }}</td>
                                    <td class="time">
                                        <span class="start"
                                              data-value="{{ $employee->start_time }}">{{ $employee->start_time }}</span>
                                        <span class="end"
                                              data-value="{{ $employee->end_time }}">{{ $employee->end_time }}</span>
                                    </td>
                                    <td class="ellipses status" data-value="{{ $employee->status }}">
                                        @if($employee->status == 0)
                                            <span class="online_circle"></span>
                                        @elseif($employee->status == 1)
                                            <span class="away_circle"></span>
                                        @elseif($employee->status == 2  )
                                            <span class="ofline_circle"></span>
                                        @endif
                                    </td>
                                    <td class="role"
                                        data-value="{{ $employee->employee_id }}">{{ $employee->employee_id }}</td>
                                    <td class="icons_text text-right">
                                        <i class="far fa-edit" data-id="{{ $employee->id }}"></i>
                                        <a class="destroy_employee" data-id="{{ $employee->id }}"><i
                                                class="fas fa-trash-alt "></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="working">
                    <div class="works">
                        <span class="online_circle1"></span>
                        <p>Works</p>
                    </div>
                    <div class="works">
                        <span class="away_circle1"></span>
                        <p>Temporarily<br/>
                            not working</p>
                    </div>
                    <div class="works">
                        <span class="ofline_circle1"></span>
                        <p>Does not work</p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <h2>Washing bay listing</h2>
                <div class="table-responsive text-nowrap">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Spot Name</th>
                            <th scope="col">Spot number</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Lorem ipsum dolor sit amet</td>
                            <td> 1</td>
                            <td class="icons_text text-right"><i class="far fa-edit"></i>
                                <i class="fas fa-trash-alt"></i>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <div class="users">
                    <h2>User listing</h2>
                    <button class="add_user" data-toggle="modal" data-target="#exampleModal">Add user</button>
                </div>
                <form action="">
                    @csrf
                    <table class="table table-responsive text-nowrap">
                        <thead>
                        <tr>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Phone number</th>
                            <th scope="col">Car license plate</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clients as $client)
                            <tr class="client{{ $client->id }}">
                                <td class="name" data-value="{{ $client->first_name }}"><span
                                        class="far fa-user"></span>
                                    {{ $client->first_name }}
                                </td>
                                <td class="last_name"
                                    data-value="{{ $client->last_name }}">{{ $client->last_name }}</td>
                                <td class="number"
                                    data-value="{{ $client->phone_number }}"> {{ $client->phone_number }}</td>

                                <td class="license"
                                    data-value="{{ $client->car_license_plate }}">{{ $client->car_license_plate }}</td>
                                <td class="icons_text text-right">
                                    <i class="far fa-edit" data-id="{{ $client->id }}"></i>
                                    <a class="destroy_client" data-id="{{ $client->id }}"><i
                                            class="fas fa-trash-alt "></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="tab-pane fade" id="v-pills-service" role="tabpanel" aria-labelledby="v-pills-service-tab">
                <h2>Washing bay listing</h2>
                <div class="table-responsive text-nowrap">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Service Name</th>
                            <th scope="col">Service Type</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Lorem ipsum dolor sit amet</td>
                            <td>
                                <select>
                                    <option value="">SUV</option>
                                    <option value="">...</option>
                                </select>
                            </td>
                            <td class="icons_text text-right"><i class="far fa-edit"></i>
                                <i class="fas fa-trash-alt"></i>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">New user</span>

                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>

                </div>
                <div class="modal-body">
                    <section class="new_employee">

                        </button>
                        <div class="content1">
                            <div class="add_photo">
                                <div class="photo">
                                    <img src="images/035-user.png">
                                </div>
                                <p>Add photo</p>
                                <span class="fas fa-plus"></span>
                            </div>
                            <form action="{{ route('add.client') }}" method="POST">@csrf
                                <input type="text" aria-describedby="emailHelp" name="first_name"
                                       placeholder="First Name *">
                                <input type="text" aria-describedby="emailHelp" name="last_name"
                                       placeholder="Last Name *">
                                <input type="text" aria-describedby="emailHelp" name="phone_number"
                                       placeholder="Phone Number *">
                                <input type="email" aria-describedby="emailHelp" name="email" placeholder="Email *">
                                <input type="text" aria-describedby="emailHelp" name="car_license_plate"
                                       placeholder="Car license plate *">

                                <button class="save">Save</button>
                            </form>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">New employee enrollment</span>

                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    <section class="new_employee">
                        <div class="content1">
                            <div class="add_photo">
                                <div class="photo">
                                    <img src="images/035-user.png">
                                </div>
                                <p>Add photo</p>
                                <span class="fas fa-plus"></span>
                            </div>
                            <form method="POST" action="{{ route('add.employee') }}">@csrf
                                <input type="text" aria-describedby="emailHelp" name="first_name"
                                       placeholder="First Name *">
                                <input type="text" aria-describedby="emailHelp" name="last_name"
                                       placeholder="Last Name *">
                                <input type="text" aria-describedby="emailHelp" name="phone_number"
                                       placeholder="Phone Number *">
                                <input type="text" aria-describedby="emailHelp" name="employee_id"
                                       placeholder="Employee id *">

                                <button class="save">Save</button>
                            </form>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>
@endsection
