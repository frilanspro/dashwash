<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>home</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css"
          integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="https://rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.css">
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new-employee.css') }}">
</head>
<body>

@include('user.layouts.header')

@yield('content')

@include('user.layouts.footer')






<script src="{{ asset('libs/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('libs/js/popper.min.js') }}"></script>
<script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
<script src="{{ asset('libs/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('libs/js/wow.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://cdn.rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.min.js"></script>
<script src="{{ asset('js/script.js') }}"></script>

<script>
    $('[data-tab]').on('click', function (e) {
        $(this).parent().parent().siblings('[data-content=' + $(this).data('tab') + ']').addClass('active').siblings('[data-content]').removeClass('active');
        console.log($(this).parent().parent().siblings('[data-content]'));
        e.preventDefault()
    });

    var categories = [
        '2019 00:00:00 GMT-0500 (EST)',
        '2019-01-02 00:00:00 GMT-0500 (EST)',
        '2019-01-03 00:00:00 GMT-0500 (EST)',
        '2019-01-04 00:00:00 GMT-0500 (EST)',
        '2017-01-05 00:00:00 GMT-0500 (EST)',
        '2017-01-06 00:00:00 GMT-0500 (EST)',
        '2017-01-07 00:00:00 GMT-0500 (EST)',
        '2017-01-08 00:00:00 GMT-0500 (EST)',
        '2017-01-09 00:00:00 GMT-0500 (EST)',
        '2017-01-10 00:00:00 GMT-0500 (EST)',
        '2017-01-11 00:00:00 GMT-0500 (EST)',
        '2017-01-12 00:00:00 GMT-0500 (EST)',
        '2017-02-01 00:00:00 GMT-0500 (EST)',
        '2017-02-02 00:00:00 GMT-0500 (EST)',
        '2017-02-03 00:00:00 GMT-0500 (EST)',
        '2017-03-12 00:00:00 GMT-0500 (EST)'
    ].map(function(date) {
        let formatOptions = { month: '2-digit', day: '2-digit', year: 'numeric' };
        return new Date(date).toLocaleDateString(undefined, formatOptions);
    });

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category',
            gridLineColor: 'red',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            categories: categories,
            dateFormat: {
                millisecond: '%H:%M:%S.%L',
                second: '%H:%M:%S',
                minute: '%H:%M',
                hour: '%H:%M',
                day: '%e. %b',
                week: '%e. %b',
                month: '%b \'%y',
                year: '%Y'
            }
        },
        yAxis: {
            min: 0,
            gridLineColor: 'transparent',
            title: {
                text: 'number of washed cars'
            },
            lineWidth: 1,
            labels:{
                enabled: false,
                gridLineColor: 'red'

            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        dateRangeGrouping: true,

        plotOptions: {
            column: {
                dataGrouping: {
                    units: [
                        ['hour', [1]],
                        ['day', [1]],
                        ['month', [1]],
                        ['year', null]
                    ],
                    groupPixelWidth: 100
                }
            }
        },
        navigator: {
            enabled: false
        },

        rangeSelector: {
            buttons: [{
                type: 'day',
                count: 1,
                text: 'Day'
            }, {
                type: 'week',
                count: 1,
                text: 'Week'
            }, {
                type: 'month',
                count: 1,
                text: 'Month'
            }]
        },
        series: [{
            name: 'Population',
            data: [
                ["8:00", 10],
                ['9:00', 14],
                ['10:00', 8],
                ['11:00', 14],
                ['12:00', 16],
                ['13:00', 9],
                ['14:00', 9],
                ['15:00', 9],
                ['16:00', 12.0],
                ['17:00', 21],
                ['18:00', 15],
                ['19:00', 15],
                ['20:00', 15],
                ['21:00', 10.6],
                ['22:00', 10.6],
                ['23:00', 7]
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}',
                y: 10,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
</script>
</body>
</html>

