<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#">
        <img src="images/Vector-Smart-Object.png">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto"></ul>
        <div class="car_wash">
            <select>
                <option value="volvo">Car wash name and address</option>
                <option value="saab">...</option>
            </select>
        </div>
        <div class="navbar-text d-flex">
            <i class="far fa-bell"></i>
            <div class="dropdown">
                <button type="button" class="dropdown_button dropdown-toggle" data-toggle="dropdown">
                    <i class="far fa-user"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Profile </a>
                    <a class="dropdown-item" href="#">Settings</a>
                </div>
            </div>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                Logout
            </a>
            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</nav>
