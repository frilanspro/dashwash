<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EditController extends Controller
{
    public function editClient(Request $request)
    {
        $data = $request->all();
        Client::where('id',$data['id'])->update($data);

    }
    public function editEmployee(Request $request)
    {
        $data = $request->all();
        Employee::where('id',$data['id'])->update($data);

    }
}
