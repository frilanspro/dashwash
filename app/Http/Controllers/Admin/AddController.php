<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddController extends Controller
{
    public function addClient (Request $request)
    {
        $data = $request->all();
        Client::create($data);
        return redirect()->back();
    }
    public function addEmployee (Request $request)
    {
        $data = $request->all();
        $data['status']= 2 ;
        Employee::create($data);
        return redirect()->back();
    }
}
