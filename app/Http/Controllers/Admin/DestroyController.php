<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DestroyController extends Controller
{
    public function destroyEmployee(Request $request)
    {

        $id = $request->all();
        Employee::where('id',$id['id'])->delete();

    }
    public function destroyClient(Request $request)
    {
        $id = $request->all();
        Client::where('id',$id['id'])->delete();

    }
}
