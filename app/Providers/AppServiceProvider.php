<?php

namespace App\Providers;

use App\Models\Client;
use App\Models\Employee;
use Illuminate\Support\Composer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**


     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
