<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use View\Connection;
use App\Models\Client;
use App\Models\Employee;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Connection::class, function ($app) {
            return new Connection(config('view'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('admin.home', function () {
            $clients = Client::get();
            $employees = Employee::get();
            View::share(['clients' => $clients, 'employees' => $employees]);
        });
    }
}
