<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $fillable = [
      'first_name',
      'last_name',
      'phone_number',
      'employee_id',
      'date',
      'start_time',
      'end_time',
      'status',
    ];
}
