<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;


// admin Routs
Route::group(['namespace' => 'Admin'], function () {
//
    Route::middleware('auth')->group( function () {
        Route::view('/', 'admin.home')->name('home');

//        Post Routs

        Route::post('/client','AddController@addClient')->name('add.client');
        Route::post('/employee','AddController@addEmployee')->name('add.employee');
        Route::delete('/destroy_employee','DestroyController@destroyEmployee')->name('destroy_employee');
        Route::delete('/destroy_client','DestroyController@destroyClient')->name('destroy_client');
        Route::post('/edit_employee','EditController@editEmployee')->name('edit_employee');
        Route::post('/edit_client','EditController@editClient')->name('edit_client');

    });

});

// User Routs
Route::group(['namespace' => 'User'], function () {

});

// Site Routs
Route::group(['namespace' => 'Site'], function () {

});

Route::get('/', function () {
    return view('sign-up');
});


Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
