$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('[data-toggle="popover"]').popover();


    var data1 = $(".datea");
    var data2 = $(".dropdata");
    var data3 = $(".datea1");
    var data4 = $(".dropdata1");
    var d = new Date();
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    // document.getElementsByClassName("datea").innerHTML = months[d.getMonth()];
    $(".datea1").text(months[d.getMonth()] + " / " + d.getDate() + " / " + d.getFullYear());
    $(".datea").text(months[d.getMonth()] + " / " + d.getDate() + " / " + d.getFullYear());


    $(".btn").on("click", function () {
        $(".datea").text($(".dropdata").text());
        $(".dropdata1").text($(".datea1").text());

    });


    $('.destroy_client').on('click', function () {
        var id = $(this).data('id');
        $.ajax({
            url: 'destroy_client',
            type: 'DELETE',
            data: {id: id},
            success: function () {
                $('.client' + id).css('display', 'none')
            }
        });
    });

    $('.destroy_employee').on('click', function () {
        var id = $(this).data('id');
        $.ajax({
            url: 'destroy_employee',
            type: 'DELETE',
            data: {id: id},
            success: function () {
                $('.employee' + id).css('display', 'none')
            }
        })
    });

    $('.edit_employee').on('click', function () {
        var id = $(this).data('id');
        var data = $('').serialize();
        $.ajax({
            url: 'edit_employee',
            type: 'Post',
            data: {id: id, data: data},
            success: function () {
                $('.employee' + id).css('display', 'none')
            }
        })
    });
    $('.edit_client').on('click', function () {
        var id = $(this).data('id');
        var data = $('').serialize();
        $.ajax({
            url: 'edit_client',
            type: 'Post',
            data: {id: id, data: data},
            success: function () {
                $('.client' + id).css('display', 'none')
            }
        })
    });

});

let CURR_DATE = new Date();

const MONTHS = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

function getTotalDaysInMonth(year, month) {

    return 32 - new Date(year, month, 32)
        .getDate();
}

const grid = document.querySelectorAll('#calendar-table td');
const dateText = document.getElementById('date-text');

grid.forEach(cell => cell.onclick = function () {
    const selectedDate = cell.innerHTML;
    if (selectedDate === '')
        return;
    CURR_DATE.setDate(selectedDate);
    renderCalendar();
});

const calendarTitle = document.querySelectorAll('#calendar-title > span')[0];

// clears all cells
function clearGrid() {
    grid.forEach(cell => {
        cell.innerHTML = '';
        cell.classList.remove('today-cell');
    });
}

function renderCalendar(date = CURR_DATE) {
    clearGrid();

    // sets month and year
    calendarTitle.innerText = `${MONTHS[date.getMonth()]}, ${date.getFullYear()}`;

    const dayOfWeek = date.getDay();
    const dateOfMnth = date.getDate();

    let totalMonthDays = getTotalDaysInMonth(
        date.getFullYear(),
        date.getMonth()
    );

    let startDay = dayOfWeek - dateOfMnth % 7 + 1;

    if (startDay < 0)
        startDay = (startDay + 35) % 7;

    for (let i = startDay; i < totalMonthDays + startDay; i++)
        grid[i % 35].innerHTML = (i - startDay + 1);

    grid[(startDay + dateOfMnth - 1) % 35].classList.add('today-cell');

    dateText.innerHTML = CURR_DATE.toLocaleDateString("en-US", {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    });

}

[...document.getElementsByClassName('btn')].forEach(btn => {

    let incr = 1;
// left button decreases month
    if (btn.classList.contains('left'))
        incr = -1;

    btn.onclick = function () {
        CURR_DATE.setMonth(CURR_DATE.getMonth() + incr);
        renderCalendar();
    };

});
//clearGrid()
renderCalendar();

$(document).ready(function () {
    $(document).on('click', '#v-pills-profile .fa-edit', function () {
        var name = $(this).closest('tr').find('.name').data('value');
        $(this).closest('tr').find('.name').html('<input type="text" name="name" value="' + name + '">');

        var last_name = $(this).closest('tr').find('.last_name').data('value');
        $(this).closest('tr').find('.last_name').html('<input type="text" name="last_name" value="' + last_name + '">');

        var date = $(this).closest('tr').find('.date').data('value');
        if (date) {
            // date = moment(date).format('YYYY-MM-DD');
        } else {
            date = "";
        }
        $(this).closest('tr').find('.date').html('<input type="date" name="date" value="' + date + '">');

        var start_time = $(this).closest('tr').find('.time .start').data('value');
        $(this).closest('tr').find('.time .start').html('<input type="time" name="start_time" value="' + start_time + '">');

        var end_time = $(this).closest('tr').find('.time .end').data('value');
        $(this).closest('tr').find('.time .end').html('<input type="time" name="end_time" value="' + end_time + '">');

        var status = $(this).closest('tr').find('.status').data('value');
        $(this).closest('tr').find('.status').html(
            '<select name="status">' +
            '<option ' + (status == 0 ? 'selected' : '') + ' value="0">Works</option>' +
            '<option ' + (status == 1 ? 'selected' : '') + ' value="1">Temporarily not working</option>' +
            '<option ' + (status == 2 ? 'selected' : '') + ' value="2">Does not work</option>' +
            '</select>');

        var role = $(this).closest('tr').find('.role').data('value');
        $(this).closest('tr').find('.role').html(
            '<select name="role">' +
            '<option ' + (role == 'Admin' ? 'selected' : '') + ' value="Admin">Admin</option>' +
            '<option ' + (role == 'Acount Manager' ? 'selected' : '') + ' value="Acount Manager">Acount Manager</option>' +
            '<option ' + (role == 'User' ? 'selected' : '') + ' value="User">User</option>' +
            '</select>');


        $(this).removeClass('fa-edit');
        $(this).addClass('fa-save');
    });
    $(document).on('click', '#v-pills-profile .fa-save', function () {
        var name = $(this).closest('tr').find('input[name="name"]').val();
        $(this).closest('tr').find('.name').html(name);

        var last_name = $(this).closest('tr').find('input[name="last_name"]').val();
        $(this).closest('tr').find('.last_name').html(last_name);

        var date = $(this).closest('tr').find('input[name="date"]').val();
        if (date) {
            // date = moment(date).format('mm dd yyyy');
        } else {
            date = "";
        }

        $(this).closest('tr').find('.date').html(date);


        var start_time = $(this).closest('tr').find('input[name="start_time"]').val();
        $(this).closest('tr').find('.time .start').html(start_time);

        var end_time = $(this).closest('tr').find('input[name="end_time"]').val();
        $(this).closest('tr').find('.time .end').html(end_time);

        var status = $(this).closest('tr').find('select[name="status"]').val();
        switch (status) {
            case "0":
                $(this).closest('tr').find('.status').html('<span class="online_circle"></span>');
                break;
            case "1":
                $(this).closest('tr').find('.status').html('<span class="away_circle"></span>');
                break;
            case "2":
                $(this).closest('tr').find('.status').html('<span class="ofline_circle"></span>');
                break;
        }

        var role = $(this).closest('tr').find('select[name="role"]').val();
        $(this).closest('tr').find('.role').html(role);
        $(this).removeClass('fa-save');
        $(this).addClass('fa-edit');
        var id = $(this).data('id');
        var data = {
            id:id,
            first_name: name,
            last_name: last_name,
            date: date,
            start_time: start_time,
            end_time: end_time,
            status:status,
            employee_id:role,
        };
        $.ajax({
            url: 'edit_employee',
            type: 'POST',
            data: data,
            success: function (data) {


            }
        })
    });


    $(document).on('click', '#v-pills-settings .fa-edit', function () {
        var name = $(this).closest('tr').find('.name').data('value');
        $(this).closest('tr').find('.name').html('<input type="text" name="name" value="' + name + '">');

        var last_name = $(this).closest('tr').find('.last_name').data('value');
        $(this).closest('tr').find('.last_name').html('<input type="text" name="last_name" value="' + last_name + '">');

        var number = $(this).closest('tr').find('.number').data('value');
        $(this).closest('tr').find('.number').html('<input type="text" name="number" value="' + number + '">');
        var license = $(this).closest('tr').find('.license').data('value');
        $(this).closest('tr').find('.license').html('<input type="text" name="license" value="' + license + '">');
        $(this).removeClass('fa-edit');
        $(this).addClass('fa-save');
    });
    $(document).on('click', '#v-pills-settings .fa-save', function () {
        var name = $(this).closest('tr').find('input[name="name"]').val();
        $(this).closest('tr').find('.name').html(name);
        var last_name = $(this).closest('tr').find('input[name="last_name"]').val();
        $(this).closest('tr').find('.last_name').html(last_name);
        var number = $(this).closest('tr').find('input[name="number"]').val();
        $(this).closest('tr').find('.number').html(number);
        var license = $(this).closest('tr').find('input[name="license"]').val();
        $(this).closest('tr').find('.license').html(license);
        $(this).removeClass('fa-save');
        $(this).addClass('fa-edit');
        var id = $(this).data('id');
        var data = {
            id:id,
            first_name: name,
            last_name: last_name,
            phone_number: number,
            car_license_plate:license,
        };
        $.ajax({
            url: 'edit_client',
            type: 'POST',
            data: data,
            success: function (data) {


            }
        })
    });
    $(document).on('click', '#v-pills-service .fa-edit', function () {
        var text = $(this).closest('tr').find('.text').data('value');
        $(this).closest('tr').find('.text').html('<input type="text" name="text" value="' + text + '">');

        var last_name = $(this).closest('tr').find('.last_name').data('value');
        $(this).closest('tr').find('.last_name').html('<input type="text" name="last_name" value="' + last_name + '">');


        $(this).removeClass('fa-edit');
        $(this).addClass('fa-save');
    });
    $(document).on('click', '#v-pills-service .fa-save', function () {
        var text = $(this).closest('tr').find('input[name="text"]').val();
        $(this).closest('tr').find('.text').html(text);

        var last_name = $(this).closest('tr').find('input[name="last_name"]').val();
        $(this).closest('tr').find('.last_name').html(last_name);


        $(this).removeClass('fa-save');
        $(this).addClass('fa-edit');
    });
    $(document).on('click', '#v-pills-messages .fa-edit', function () {
        var text = $(this).closest('tr').find('.text').data('value');
        $(this).closest('tr').find('.text').html('<input type="text" name="text" value="' + text + '">');
        var numbers = $(this).closest('tr').find('.numbers').data('value');
        $(this).closest('tr').find('.numbers').html('<input type="number" name="numbers" value="' + numbers + '">');


        $(this).removeClass('fa-edit');
        $(this).addClass('fa-save');
    });
    $(document).on('click', '#v-pills-messages .fa-save', function () {

        var text = $(this).closest('tr').find('input[name="text"]').val();
        $(this).closest('tr').find('.text').html(text);

        var numbers = $(this).closest('tr').find('input[name="numbers"]').val();
        $(this).closest('tr').find('.numbers').html(numbers);


        $(this).removeClass('fa-save');
        $(this).addClass('fa-edit');
    });
    $(document).on('click', '#messages .fa-edit', function () {
        var name = $(this).closest('tr').find('.name').data('value');
        $(this).closest('tr').find('.name').html('<input type="text" name="name" value="' + name + '">');

        var license = $(this).closest('tr').find('.license').data('value');
        $(this).closest('tr').find('.license').html('<input type="text" name="license" value="' + license + '">');


        $(this).removeClass('fa-edit');
        $(this).addClass('fa-save');
    });
    $(document).on('click', '#messages .fa-save', function () {

        var name = $(this).closest('tr').find('input[name="name"]').val();
        $(this).closest('tr').find('.name').html(name);

        var license = $(this).closest('tr').find('input[name="license"]').val();
        $(this).closest('tr').find('.license').html(license);


        $(this).removeClass('fa-save');
        $(this).addClass('fa-edit');
    });
});














